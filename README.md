# Front-end Boilerplate using Sass and Gulp 4

- Run `npm install`
- Run `gulp` to run the default Gulp task

In this proejct, Gulp is configured to run the following functions:

- Compile the SCSS files to CSS
- Autoprefix and minify the CSS file
- Concatenate the JS files
- Uglify the JS files
- Move final CSS and JS files to the `/app` folder
