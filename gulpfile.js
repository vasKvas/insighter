// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require("gulp");
// Importing all the Gulp-related packages we want to use
const sourcemaps = require("gulp-sourcemaps");
const browsersync = require("browser-sync").create();
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const babel = require("gulp-babel");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const imagemin = require("gulp-imagemin");
const replace = require("gulp-replace");

// File paths
const files = {
  scssPath: "src/scss/**/*.scss",
  jsPath: "src/js/**/*.js",
  imgPath: "src/images/**/*",
  videoPath: "src/video/**/*",
  fontsPath: "src/fonts/**/*",
  jqueryNodePath: "node_modules/jquery/dist/jquery.min.js",
  jqueryValidationPath:
    "node_modules/jquery-validation/dist/jquery.validate.min.js",
  jqueryInputMaskPath: "node_modules/inputmask/dist/jquery.inputmask.min.js",
  slickCarouselCss: "node_modules/slick-carousel/slick/slick.css",
  slickCarouselJs: "node_modules/slick-carousel/slick/slick.min.js",
  waypoints: "node_modules/waypoints/lib/jquery.waypoints.min.js",
  svgMapCss: "src/svgMap/dist/svgMap.min.css",
  svgMapJs: "src/svgMap/dist/svgMap.min.js",
};

function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: "./",
    },
    port: 8000,
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}
// Sass task: compiles the style.scss file into style.css
function scssTask() {
  return src(files.scssPath)
    .pipe(sourcemaps.init()) // initialize sourcemaps first
    .pipe(sass()) // compile SCSS to CSS
    .pipe(postcss([autoprefixer(), cssnano()])) // PostCSS plugins
    .pipe(sourcemaps.write(".")) // write sourcemaps file in current directory
    .pipe(dest("app/css"))
    .pipe(browsersync.stream());
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask() {
  return src([
    files.jsPath,
    //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
  ])
    .pipe(concat("all.js"))
    .pipe(
      babel({
        presets: ["@babel/env"],
      })
    )
    .pipe(uglify())
    .pipe(dest("app/js"))
    .pipe(browsersync.stream());
}

// Optimize Images
function imageOptimize() {
  return src([files.imgPath])
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
              collapseGroups: true,
            },
          ],
        }),
      ])
    )
    .pipe(dest("app/images"))
    .pipe(browsersync.stream());
}

function copyFiles() {
  return src([
    files.jqueryNodePath,
    files.jqueryValidationPath,
    files.jqueryInputMaskPath,
    files.slickCarouselCss,
    files.slickCarouselJs,
    files.waypoints,
    files.svgMapCss,
    files.svgMapJs,
  ]).pipe(dest("app/vendor"));
}

function copyFonts() {
  return src([files.fontsPath]).pipe(dest("app/fonts"));
}

function copyVideo() {
  return src([files.videoPath]).pipe(dest("app/video"));
}

// Cachebust
var cbString = new Date().getTime();
function cacheBustTask() {
  return src(["index.html"])
    .pipe(replace(/cb=\d+/g, "cb=" + cbString))
    .pipe(dest("."));
}

// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask() {
  watch(files.scssPath, scssTask);
  watch(files.jsPath, jsTask);
  watch(files.imgPath, imageOptimize);
  watch("./.html", series(cacheBustTask, browserSyncReload));
}

const build = series(
  copyFonts,
  copyVideo,
  copyFiles,
  parallel(scssTask, jsTask, imageOptimize, watchTask, browserSync)
);
const watchFiles = parallel(watchTask, browserSync);

exports.build = build;
exports.watch = watchFiles;
exports.default = build;
