jQuery(document).ready(function ($) {
  const $whySlider = $("#why-slider");
  const $clientImages = $("#clients-images");
  const $clientDescription = $("#clients-description");
  const $otherServices = $("#other-services-slider");
  const $serviceIncludes = $("#includes-slider");

  if ($whySlider) {
    $whySlider.slick({
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      speed: 300,
      autoplaySpeed: 3000,
      touchThreshold: 30,
      swipeToSlide: true,

      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 700,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplaySpeed: 5000,
          },
        },
      ],
    });
  }
  if ($clientImages) {
    $clientImages.slick({
      dots: true,
      arrows: false,
      infinite: false,
      fade: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: "#clients-description",
    });
    $clientImages.on("beforeChange", function (
      event,
      slick,
      currentSlide,
      nextSlide
    ) {
      $(".js-client-item").removeClass("active");
      $('.js-client-item[data-index="' + nextSlide + '"]').addClass("active");
    });
  }
  if ($serviceIncludes) {
    $serviceIncludes.slick({
      dots: true,
      arrows: false,
      infinite: false,
      fade: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            fade: false,
          },
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: false,
            infinite: true,
          },
        },
      ],
    });
    $serviceIncludes.on("beforeChange", function (
      event,
      slick,
      currentSlide,
      nextSlide
    ) {
      $(".js-includes-item").removeClass("active");
      $('.js-includes-item[data-index="' + nextSlide + '"]').addClass("active");
    });
  }

  if ($clientDescription) {
    $clientDescription.slick({
      dots: false,
      arrows: false,
      infinite: false,
      speed: 300,
      touchThreshold: 30,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: "#clients-images",
    });
  }
  if ($otherServices) {
    $otherServices.slick({
      dots: false,
      arrows: false,
      infinite: false,
      speed: 300,
      touchThreshold: 30,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
            infinite: true,
          },
        },
        {
          breakpoint: 700,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            speed: 400,
            autoplaySpeed: 4000,
            infinite: true,
          },
        },
      ],
    });
  }
});
