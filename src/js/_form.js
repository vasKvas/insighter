jQuery(document).ready(function ($) {
  function updateModalForm() {
    $("#modal-form").show();
    $("#success-msg").hide();
  }

  function openModal() {
    $("#modal").fadeIn();
    $("body").addClass("opened-modal");
  }

  function closeModal() {
    $("#modal").fadeOut();
    $("body").removeClass("opened-modal");
    setTimeout(updateModalForm, 500);
  }

  function showErrorMessage() {
    const $error = $("#server-error-msg");
    $error.fadeIn();
    setTimeout(function () {
      $error.fadeOut();
    }, 3000);
  }
$(".js-modal-close").click(function () {
    closeModal();
  });

  $(".js-modal-open").click(function () {
    const buttonText = $(this).text();
    const buttonType = $(this).data('type');
    $('.form-submit').text(buttonText);
    $('.form-title').hide();
    $('.form-description').hide();
    $(`.form-title-${buttonType}`).show();
    $(`.form-description-${buttonType}`).show();
    openModal();
  });
  
  const formValidationInit = (formId) => {
    const $form = $(`#${formId}`);
    if (!$form) return null;

    $("#phone").inputmask("[+]999999999999999", { placeholder: "" });

    $form.validate({
      submitHandler: function (form) {
        const data = {
          name: $("#name").val(),
          email: $("#email").val(),
          company: $("#company").val(),
          phone: $("#phone").val(),
          message: $("#message").val(),
        };

        $.ajax({
          type: "POST",
          url: "example.com/api/",
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          data: JSON.stringify(data),
          success: function (res) {
            if (res.code == 200) {
              $form.hide();
              $form[0].reset();
              $(".transform").removeClass("active");
              $("#success-msg").fadeIn();
              setTimeout(closeModal, 4000);
            } else {
              showErrorMessage();
            }
          },
          error: function () {
            /* use showErrorMessage fn*/
            //showErrorMessage();
            /*--------------*/

            /*remove this code*/
            $form.hide();
            $form[0].reset();
            $(".transform").removeClass("active");
            $("#success-msg").fadeIn();
            setTimeout(closeModal, 4000);
            /*--------------*/
          },
        });
        return false;
      },
    });
  };

  formValidationInit("modal-form");
  $(".label-transform")
    .focusin(function () {
      $(this).siblings(".transform").addClass("active");
    })
    .focusout(function () {
      if (!$(this).val()) {
        $(this).siblings(".transform").removeClass("active");
      }
    });
});
