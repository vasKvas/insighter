const pathPositions = {
  healthcare: {
    obj: ".st0",
    x: 5,
    y: 2,
  },
  automotive: {
    obj: ".st3",
    x: -8,
    y: 8,
  },
  services: {
    obj: ".st4",
    x: -10,
    y: 1,
  },
  industrial: {
    obj: ".st1",
    x: -15,
    y: -12,
  },
  oil: {
    obj: ".st5",
    x: -6,
    y: -12,
  },
  agriculture: {
    obj: ".st2",
    x: -1.5,
    y: -8,
  },
};

function animateDiagrame() {
  gsap.to(".st0", { opacity: 1, x: 0, y: 0, scale: 1, duration: 0.5 });
  gsap.to(".st3", {
    opacity: 1,
    x: 0,
    y: 0,
    scale: 1,
    duration: 0.5,
    delay: 0.2,
  });
  gsap.to(".st4", {
    opacity: 1,
    x: 0,
    y: 0,
    scale: 1,
    duration: 0.5,
    delay: 0.4,
  });
  gsap.to(".st1", {
    opacity: 1,
    x: 0,
    y: 0,
    scale: 1,
    duration: 0.5,
    delay: 0.6,
  });
  gsap.to(".st5", {
    opacity: 1,
    x: 0,
    y: 0,
    scale: 1,
    duration: 0.5,
    delay: 0.7,
  });
  gsap.to(".st2", {
    opacity: 1,
    x: 0,
    y: 0,
    scale: 1,
    duration: 0.5,
    delay: 0.8,
  });
}

function animateDiagrameList() {
  gsap.to(".item-healthcare", { opacity: 1, duration: 0.3, delay: 0.3 });
  gsap.to(".item-automotive", { opacity: 1, duration: 0.3, delay: 0.5 });
  gsap.to(".item-services", { opacity: 1, duration: 0.3, delay: 0.7 });
  gsap.to(".item-industrial", { opacity: 1, duration: 0.3, delay: 0.9 });
  gsap.to(".item-oil", { opacity: 1, duration: 0.3, delay: 1 });
  gsap.to(".item-agriculture", { opacity: 1, duration: 0.3, delay: 1.1 });
}

function pathAnimation(path) {
  const { obj, x, y } = pathPositions[path];
  gsap.to(obj, {
    x,
    y,
    duration: 0.3,
    delay: 0,
  });
}

function setDefaultPosition(path) {
  const { obj } = pathPositions[path];
  gsap.to(obj, { x: 0, y: 0, duration: 0.3, delay: 0 });
}
