const countryValues = {
  US: { projects: "100+" },
  GB: { projects: "50+" },
  DE: { projects: "50+" },
  FR: { projects: "50+" },
  IT: { projects: "50+" },
  ES: { projects: "50+" },
  NL: { projects: "20+" },
  DK: { projects: "20+" },
  SE: { projects: "20+" },
  NO: { projects: "5" },
  FI: { projects: "5" },
  AT: { projects: "10+" },
  CH: { projects: "20+" },
  GR: { projects: "5" },
  LU: { projects: "3" },
  PL: { projects: "20+" },
  HU: { projects: "10+" },
  RO: { projects: "10+" },
  CZ: { projects: "10+" },
  SK: { projects: "10+" },
  RU: { projects: "100+" },
  UA: { projects: "50+" },
  EE: { projects: "2" },
  LV: { projects: "5" },
  LT: { projects: "5" },
  BA: { projects: "2" },
  GE: { projects: "5" },
  MD: { projects: "2" },
  AZ: { projects: "5" },
  ME: { projects: "1" },
  RS: { projects: "2" },
  BY: { projects: "5" },
  TR: { projects: "10+" },
  CN: { projects: "20+" },
  IN: { projects: "50+" },
  TH: { projects: "10+" },
  ID: { projects: "10+" },
  KR: { projects: "20+" },
  JP: { projects: "10+" },
  TW: { projects: "10+" },
  LB: { projects: "3" },
  SG: { projects: "20+" },
  HK: { projects: "10+" },
  IQ: { projects: "1" },
  IR: { projects: "1" },
  SA: { projects: "5" },
  AE: { projects: "5" },
  VN: { projects: "3" },
  CA: { projects: "20+" },
  MX: { projects: "20+" },
  BR: { projects: "50+" },
  AR: { projects: "10+" },
  PY: { projects: "3" },
  PY: { projects: "3" },
  PE: { projects: "3" },
  EC: { projects: "1" },
  NG: { projects: "2" },
  EG: { projects: "5" },
  DZ: { projects: "2" },
  ZA: { projects: "5" },
  CT: { projects: "1" },
  MA: { projects: "5" },
  KE: { projects: "2" },
  AU: { projects: "10+" },
};

const svgMapDataGPD = {
  data: {
    projects: {
      name: "Projects completed",
      format: "{0}",
      thousandSeparator: ",",
      thresholdMax: 200,
      thresholdMin: 1,
    },
  },
  applyData: "projects",
  values: countryValues,
};

class ProjectsMap {
  constructor() {
    this.animationCountries = {
      US: { projects: "100+" },
      CA: { projects: "20+" },
      MX: { projects: "20+" },
      BR: { projects: "50+" },
      AR: { projects: "10+" },
      PE: { projects: "3" },
      RU: { projects: "100+" },
      CN: { projects: "20+" },
      IN: { projects: "50+" },
      AU: { projects: "10+" },
      DZ: { projects: "2" },
      SA: { projects: "5" },
      IR: { projects: "1" },
      UA: { projects: "50+" },
      GB: { projects: "50+" },
      DE: { projects: "50+" },
      FR: { projects: "50+" },
      PL: { projects: "20+" },
    };
    this.animationIndex = 0;
  }

  init(mapContainer, data) {
    new svgMap({
      targetElementID: mapContainer,
      data: data,
      colorMax: "#f26522",
      colorMin: "#f38f6c",
      colorNoData: "#3350a2",
      noDataText: "",
      mouseWheelZoomEnabled: false,
    });
  }

  startAnimation() {
    const activeCountries = Object.keys(this.animationCountries);

    this.intervalAnimation = setInterval(() => {
      this.activePath = document.getElementById(
        `svgMap-map-country-${activeCountries[this.animationIndex]}`
      );
      this.activePath.classList.add("active");
      setTimeout(() => {
        this.activePath.classList.remove("active");
        this.animationIndex =
          this.animationIndex < activeCountries.length - 1
            ? this.animationIndex + 1
            : 0;
      }, 1500);
    }, 1700);
  }
  stopAnimation() {
    clearInterval(this.intervalAnimation);
  }
}

const map = new ProjectsMap();
const initSvgMap = (mapContainerId) => {
  const mapContainer = document.getElementById(mapContainerId);
  if (mapContainer) {
    map.init(mapContainerId, svgMapDataGPD);
  } else return null;
};

initSvgMap("svgMap");
