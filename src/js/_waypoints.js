jQuery(document).ready(function ($) {
  function animationInit(el, offset) {
    const $animationPoint = $(el);
    if ($animationPoint) {
      $animationPoint.waypoint(
        function () {
          $(this.element).addClass("animate");
        },
        {
          offset: offset,
        }
      );
    }
    return null;
  }

  $(".b-main").waypoint(
    function () {
      $("body").toggleClass("header-stick");
    },
    {
      offset: "-80px",
    }
  );

  if ($(".js-animation-diagrame")) {
    $(".js-animation-diagrame").waypoint(
      function () {
        animateDiagrame();
        animateDiagrameList();
      },
      {
        offset: "50%",
      }
    );
  }

  if ($(".section-services")) {
    $(".section-services").waypoint(
      function () {
        $("body").toggleClass("show-action-buttons");
      },
      {
        offset: "0px",
      }
    );
  }

  if ($("#svgMap")) {
    $("#svgMap").waypoint(
      function (direction) {
        if (direction === "down") {
          map.stopAnimation();
        }
        if (direction === "up") {
          map.startAnimation();
        }
      },
      {
        offset: "-99%",
      }
    );
    $("#svgMap").waypoint(
      function (direction) {
        if (direction === "down") {
          map.startAnimation();
        }
        if (direction === "up") {
          map.stopAnimation();
        }
      },
      {
        offset: "99%",
      }
    );
  }

  animationInit(".animation-point", "70%");
});
