jQuery(document).ready(function ($) {
  function setActiveService() {
    const $servicesList = $(".js-services-hover");
    const $serviceItem = $(".js-services-hover li");
    $serviceItem.hover(function () {
      $serviceItem.removeClass("active");
      $(this).addClass("active");
    });

    $servicesList.mouseleave(function () {
      $serviceItem.removeClass("active");
      $("#service-expert-network").addClass("active");
    });
  }

  $(".js-path")
    .mouseenter(function () {
      const dataPath = $(this).data("path");
      pathAnimation(dataPath);
      $(`.b-data__item[data-slug="${dataPath}"]`).addClass("active");
    })
    .mouseleave(function () {
      const dataPath = $(this).data("path");
      setDefaultPosition(dataPath);
      $(".b-data__item").removeClass("active");
    });

  $(".b-data__item")
    .mouseenter(function () {
      const dataSlug = $(this).data("slug");
      $(".b-data__item").removeClass("active");
      $(this).addClass("active");
      pathAnimation(dataSlug);
    })
    .mouseleave(function () {
      const dataSlug = $(this).data("slug");
      $(".b-data__item").removeClass("active");
      setDefaultPosition(dataSlug);
    });

  $(".js-scroll-to").click(function (e) {
    e.preventDefault();
    const dataHref = $(this).attr("href");
    const dataScroll = $(this).data("scroll");
    if (dataScroll && $(`${dataScroll}`)) {
      let offset = $(`${dataScroll}`).offset().top - 70;
      $("html, body").animate(
        {
        scrollTop: offset,
        },
        1000
      );
    } else {
      window.location = dataHref;
    }
    
  });

  
  if ($(".js-services-hover")) {
    setActiveService();
  }

  

  $(".js-data-title").mouseenter(function () {
    const dataSlug = $(this).data("slug");
    $(this).parent().addClass("active");

    $(`#${dataSlug}`).addClass("active");
  });

  $(".path-item").mouseenter(function () {
    const pathId = $(this).attr("id");
    $(".diagrame").addClass("focus");
    $(this).addClass("active");
    $(`.js-data-title[data-slug="${pathId}"]`)
      .parent(".b-data__item")
      .addClass("active");
  });
  $(".path-item").mouseleave(function () {
    $(".diagrame").removeClass("focus");
    $(this).removeClass("active");
    $(".b-data__item").removeClass("active");
  });
  $(".js-header-modal").hover(function () {
    $(".js-header-modal").removeClass("active");
    $(this).addClass("active");
  });

  $(".js-client-item").click(function () {
    const slideIndex = $(this).data("index");
    $("#clients-images").slick("slickGoTo", +slideIndex, false);
  });

  $(".js-includes-item").click(function () {
    const slideIndex = $(this).data("index");
    $("#includes-slider").slick("slickGoTo", +slideIndex, false);
  });
  

  $(".js-menu-open").click(function () {
    $(".header-nav-wrap").fadeIn();
  });
  $(".js-menu-close").click(function () {
    $(".header-nav-wrap").fadeOut();
  });

  const autoplayVideo = (el) => {
    const video = document.getElementById(el);
    if (!video) return null;

    video.play();
    video.autoplay = true;
    video.loop = true;
    video.muted = true;
    return video;
  };
  autoplayVideo("intro-video");
});
